#ifndef ARTICLE_H
#define ARTICLE_H

#include <string>

struct Article {

   Article(const size_t _id, const std::string& _title, const std::string& _author, const std::string& _text) :
      id(_id), title(_title), author(_author), text(_text) {};
   Article() {};
   size_t id;
   std::string title;
   std::string author;
   std::string text;
};

#endif
