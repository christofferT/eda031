#include "database_file.h"
#include <unistd.h>
#include <dirent.h>
#include <vector>
#include <sys/stat.h>
#include <sys/types.h>  
#include <algorithm>
#include <utility>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "article.h"
#include "newsgroup.h"

using namespace std;

DatabaseFile::DatabaseFile() : root("db") {
   newsgroup_id = 0;
   article_id = 0;
   mkdir(root.c_str(), 0777);

}

struct my_ng_function
{

   inline bool operator() (const Newsgroup& ng, const Newsgroup& ng2){
      return (ng.id < ng2.id);
   }
};

struct my_art_function
{

   inline bool operator() (const Article& art, const Article& art2){
      return (art.id < art2.id);
   }
};

vector<Newsgroup> DatabaseFile::list_newsgroups() const{

   vector<Newsgroup> ret;
   DIR           *d;
   struct dirent *dir;
   d = opendir(root.c_str());
   if (d){  

      string newsgroupstring;
      size_t id;
      string name;
      while ((dir = readdir(d)) != NULL)
      {
         newsgroupstring = dir->d_name;
         if(!(newsgroupstring == "." || newsgroupstring == "..")){
            istringstream iss(newsgroupstring);
            iss >> id >> name;
            string extra;
            while(iss >> extra){
               name += " ";
               name += extra;
            }

            name.erase(0,1);
            Newsgroup ng(name, id);
            ret.push_back(ng);
         }
      }
      closedir(d);

   }
   sort(ret.begin(), ret.end(), my_ng_function());
   return ret;
}



vector<Article> DatabaseFile::list_articles(const size_t id) const {

   vector<Article> arts;
   vector<Newsgroup> newsgroups = list_newsgroups();

   auto found = find_if(newsgroups.begin(), newsgroups.end(), [&] (Newsgroup newsgroup) {return newsgroup.id == id;});
   if(found == newsgroups.end()){
      return arts;
   } else {

      Newsgroup ng = *found;  
      ostringstream oss;
      oss << root << "/" << id << "_" << ng.name << "/";
      string ngdir = oss.str();
      DIR           *d;
      struct dirent *dir;
      d = opendir(ngdir.c_str());
      if (d){  
         string articlestring;
         size_t art_id;
         string title;
         string author;
         string text;
         while ((dir = readdir(d)) != NULL)
         {
            articlestring = dir->d_name;
            if(!(articlestring == ".." || articlestring == ".")){
               ostringstream oss;
               oss << ngdir << articlestring;
               string path = oss.str();
               string line;
               istringstream iss(articlestring);
               iss >> art_id >> title;
               string extra;
               while(iss >> extra){
                  title += " ";
                  title += extra;
               }
               ifstream myfile (path.c_str());
               ostringstream oss2;
               if (myfile.is_open())
               {
                  getline(myfile, line);
                  string extra2;
                  istringstream iss(line);
                  iss >> author;
                  while( iss >> extra2){
                     author += " ";
                     author += extra2;
                  }
                  string line2;

                  ostringstream oss3;
                  getline(myfile, line2);
                  oss3 << line2;
                  while ( getline (myfile,line2) )
                  {
                     oss3 << "\n";
                     oss3 << line2; 

                  }         
                  text = oss3.str();
                  Article art;
                  art.id = art_id;
                  title.erase(0,1);
                  art.title = title;
                  art.author = author;
                  art.text = text;
                  arts.push_back(art);

                  myfile.close();
               }

            }
         }

         closedir(d);
      }
   }
   sort(arts.begin(), arts.end(), my_art_function());
   return arts;

}
bool DatabaseFile::create_newsgroup(const string& name){

   vector<Newsgroup> ngs = list_newsgroups();
   for( Newsgroup ng : ngs){ 
      if(ng.id > newsgroup_id ){
         newsgroup_id = ng.id;
      }

   }
   vector<Newsgroup> newsgroups = list_newsgroups();
   auto found = find_if(newsgroups.begin(), newsgroups.end(), [&] (Newsgroup newsgroup) {return newsgroup.name == name;});
   if(found != newsgroups.end()){
      return false;
   } else {
      ostringstream oss;
      ++newsgroup_id;
      oss << root << "/" << newsgroup_id << "_" << name;
      string dir = oss.str();
      mkdir(dir.c_str(), 0777);
      return true;
   }
}
bool DatabaseFile::delete_newsgroup(const size_t id){
   vector<Newsgroup> newsgroups = list_newsgroups();
   auto found = find_if(newsgroups.begin(), newsgroups.end(), [&] (Newsgroup newsgroup) {return newsgroup.id == id;});
   if(found == newsgroups.end()){
      return false;
   } else {
      Newsgroup ng = *found;
      ostringstream oss;
      oss  << root << "/" << ng.id << "_" << ng.name; 
      string filename = oss.str();
      DIR           *d;
      struct dirent *dir;
      d = opendir(filename.c_str());
      if (d)
      {
         cout << "i first if" << endl;
         while ((dir = readdir(d)) != NULL)
         {
            ostringstream oss2;
            oss2 << filename << "/" <<  dir->d_name;
            string filetoremove = oss2.str();
            remove(filetoremove.c_str());
         }

         closedir(d);
      } else {
         cout << "fail to open d" << endl;
      }
      rmdir(filename.c_str());
      return true;
   }
}

pair<int, Article> DatabaseFile::get_article(const size_t newsgroup_id, const size_t article_id) const{
   Article art;
   vector<Article> arts = list_articles(newsgroup_id);
   auto found = find_if(arts.begin(), arts.end(), [&] (Article article) {return article.id == article_id;});
   if(found == arts.end()){
      return make_pair(0,art);
   } else {
      art = *found;
      cout << "art.author " << art.author << endl;
      cout << "art.text " << art.text << endl;
      return make_pair(1,art);
   }
}

bool DatabaseFile::create_article(const size_t newsgroup_id, const string& title, const string& author, const string& text){

   vector<Newsgroup> newsgroups = list_newsgroups();
   auto found = find_if(newsgroups.begin(), newsgroups.end(), [&] (Newsgroup newsgroup) {return newsgroup.id == newsgroup_id;});
   if(found == newsgroups.end()){
      return false;
   } else {
      Newsgroup ng = *found;
      ostringstream oss;
      ++article_id;
      oss << root << "/" << newsgroup_id << "_" << ng.name << "/" << article_id << "_" << title;
      cout << oss.str() << endl;
      ofstream myfile;
      myfile.open(oss.str());
      myfile << author << "\n";
      myfile << text << "\n";
      myfile.close();
      return true;
   }
}

int DatabaseFile::delete_article(const size_t newsgroup_id, const size_t article_id) { 
   vector<Article> arts = list_articles(newsgroup_id);
   if(arts.size() == 0){
      return -1;
   }
   auto found = find_if(arts.begin(), arts.end(), [&] (Article article) {return article.id == article_id;});
   if(found == arts.end()){
      return -2; 
   } else {
      vector<Newsgroup> newsgroups = list_newsgroups();
      auto foundng = find_if(newsgroups.begin(), newsgroups.end(), [&] (Newsgroup newsgroup) {return newsgroup.id == newsgroup_id;});
      Newsgroup ng = *foundng;
      Article art = *found;
      ostringstream oss;
      oss  << root << "/" << ng.id << "_" << ng.name; 
      string filename = oss.str();
      cout << "filename " << filename << endl;
      DIR           *d;
      struct dirent *dir;
      d = opendir(filename.c_str());
      if (d)
      {
         cout << "inne i if" << endl;
         ostringstream oss2;
         oss2 << filename << "/" << article_id << "_" << art.title;
         string file_to_remove = oss2.str();
         cout << "file to remove " << file_to_remove << endl;
         while ((dir = readdir(d)) != NULL)
         {
            remove(file_to_remove.c_str());
         }

         closedir(d);

      }
   }

   return 0;
}



