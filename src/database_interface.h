#ifndef DATABASE_INTERFACE_H
#define DATABASE_INTERFACE_H
#include <vector>
#include <string>
#include <utility>
#include "newsgroup.h"
#include "article.h"

class DatabaseInterface {

   public:
      virtual std::vector<Newsgroup> list_newsgroups() const = 0;
      virtual std::vector<Article> list_articles(const size_t article_id) const = 0;
      virtual bool create_newsgroup(const std::string& newsgroup_name) = 0;
      virtual bool create_article(const size_t newsgroup_id, const std::string& title, const std::string& author, const std::string& text) = 0;
      virtual bool delete_newsgroup(const size_t newsgroup_id) = 0;
      virtual std::pair<int,Article> get_article(const size_t newsgroup_id, const size_t article_id) const = 0 ;
      virtual int delete_article(const size_t newsgroup_id, const size_t article_id) = 0;
};
#endif
