#include "database_memory.h"
#include <vector>
#include <iostream>
#include <utility>
#include <algorithm>
#include "article.h"
#include "newsgroup.h"

using namespace std;

DatabaseMemory::DatabaseMemory() {
   newsgroup_id = 1; 
   article_id = 1;
}

vector<Newsgroup> DatabaseMemory::list_newsgroups() const{
   return newsgroups;
}

vector<Article> DatabaseMemory::list_articles(const size_t id) const {

   auto found = articles.find(id);
   if ( found != articles.end()) {
       return found->second;
      } else {
       vector<Article> emptylist;
       return emptylist;
   }
    
}

bool DatabaseMemory::create_newsgroup(const string& name){
   auto found = find_if(newsgroups.begin(), newsgroups.end(), [&] (Newsgroup newsgroup) {return newsgroup.name == name;});
   if( found == newsgroups.end()){
      articles[newsgroup_id];
      Newsgroup curr(name, newsgroup_id);
      newsgroups.push_back(curr);
      ++newsgroup_id;
      cout << "DB reports NG created, size is: " << newsgroups.size() << endl;
      return true;
   }
   return false;
}

bool DatabaseMemory::delete_newsgroup(const size_t id){
   auto found = find_if(newsgroups.begin(), newsgroups.end(), [&] (Newsgroup newsgroup) {return newsgroup.id== id;});
   if( found == newsgroups.end()){
      return false;
   }
   newsgroups.erase(found);
   articles.erase(id);
   return true;
}

pair<int, Article> DatabaseMemory::get_article(const size_t newsgroup_id, const size_t article_id) const{
   Article emptyArticle;
   auto found = articles.find(newsgroup_id);
   if (found == articles.end() ) {
      pair<int, Article> ret = make_pair(0, emptyArticle); // Newsgroup doesn't exist.
      return ret;
   } else {
      vector<Article> list = found->second;
      auto exists = find_if(list.begin(), list.end(), [&] (Article art) { return art.id == article_id;});
      if( exists != list.end() ) {
         pair<int, Article> ret = make_pair(1, *exists); // Both exist.
         return ret;
      } 
   }
   pair<int, Article> ret = make_pair(2, emptyArticle);
   return ret; // Article doesnt exist
}

bool DatabaseMemory::create_article(const size_t newsgroup_id, const string& title, const string& author, const string& text){
   auto found = articles.find(newsgroup_id);
   if(found != articles.end()){
      vector<Article>& list = found->second;
      Article art(article_id, title, author, text);
      list.push_back(art);
      ++article_id;
      return true;
   }
   return false;
}

int DatabaseMemory::delete_article(const size_t newsgroup_id, const size_t article_id) {
   auto found = articles.find(newsgroup_id);
   if(found == articles.end()){
      return -1; // Newsgroup doesnt exist
   } 
   vector<Article>& list = found->second;
   auto exists = find_if(list.begin(), list.end(), [&] (Article art) {return art.id == article_id;});
   if(exists != list.end()){
      list.erase(exists);
      return 0 ; // Article and newsgroup exists;
   }
   return -2; // Article doesn't exist.;
}




