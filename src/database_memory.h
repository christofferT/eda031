#ifndef DATABASE_MEMORY_H
#define DATABASE_MEMORY_H
#include <vector>
#include <map>
#include <string>
#include "database_interface.h"
#include "newsgroup.h"
#include "article.h"
#include <utility>

class DatabaseMemory : public DatabaseInterface {
   public:
      DatabaseMemory();
      std::vector<Newsgroup> list_newsgroups() const;
      std::vector<Article> list_articles(const size_t id) const;
      bool create_newsgroup(const std::string& name);
      bool delete_newsgroup(const size_t id);
      std::pair<int, Article> get_article(const size_t newsgroup_id, const size_t article_id) const;
      bool create_article(const size_t newsgroup_id, const std::string& title, const std::string& author, const std::string& text);
      int delete_article(const size_t newsgroup_id, const size_t article_id);

   private:

      std::vector<Newsgroup> newsgroups;
      std::map<size_t, std::vector<Article>> articles;
      size_t newsgroup_id;
      size_t article_id;
};

#endif
