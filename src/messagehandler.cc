#include <sstream>
#include "messagehandler.h"
#include <iostream>
#include "protocol.h"


   size_t MessageHandler::read_number(const Connection* conn) {
//      read_command(conn);
      return read_integer(conn);
   }

   size_t MessageHandler::read_command(const Connection* conn) {
      return conn->read();
   }
   void MessageHandler::write_command(const Connection* conn, const size_t cmd) {
      conn->write(cmd);
   }
   void MessageHandler::write_number(const Connection* conn, const size_t nbr) {
      write_integer(conn, nbr);
   }

   void MessageHandler::write_string(const Connection* conn, const string& s) {
      MessageHandler::write_integer(conn, s.size());
      for (auto i = s.begin(); i != s.end(); ++i) {
         conn->write(*i);
      }
   }

   string MessageHandler::read_string(const Connection* conn) {
      int size = MessageHandler::read_integer(conn);
      ostringstream oss;
      for (int i = 0; i < size; ++i) {
         oss << conn->read();
      }
     return oss.str();
   }

   int MessageHandler::read_integer(const Connection* conn) {
      unsigned char c1 = conn->read();
      unsigned char c2 = conn->read();
      unsigned char c3 = conn->read();
      unsigned char c4 = conn->read();
      return c1 << 24 | c2 << 16 | c3 << 8 | c4;
   }
   void MessageHandler::write_integer(const Connection* conn, const int i) {
      conn->write((i >> 24) & 0xFF);
      conn->write((i >> 16) & 0xFF);
      conn->write((i >> 8) & 0xFF);
      conn->write((i >> 0) & 0xFF);
   }


