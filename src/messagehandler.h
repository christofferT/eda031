#ifndef MESSAGE_HANDLER_H
#define MESSAGE_HANDLER_H
#include <string>
#include "connection.h"

using namespace std;
   class MessageHandler {
      public:
         static size_t read_command(const Connection* conn);
         static size_t read_number(const Connection* conn);
         static string read_string(const Connection* conn);
         static void write_command(const Connection* conn, const size_t cmd);
         static void write_number(const Connection* conn, const size_t n);
         static void write_string(const Connection* conn, const string& s);
      private:
         static int read_integer(const Connection* conn);
         static void write_integer(const Connection* conn, const int i);
   };
#endif
