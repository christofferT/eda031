/* myclient.cc: sample client program */
#include "connection.h"
#include "connectionclosedexception.h"
#include "protocol.h"
#include "messagehandler.h"
#include "article.h"
#include "newsgroup.h"
#include <iostream>
#include <memory>
#include <string>
#include <stdexcept>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <map>
using namespace std;

void create_newsgroup(Connection& conn,string& newsgroup_name){

    MessageHandler::write_command(&conn, Protocol::COM_CREATE_NG);
    MessageHandler::write_command(&conn, Protocol::PAR_STRING);
    MessageHandler::write_string(&conn, newsgroup_name);
    MessageHandler::write_command(&conn, Protocol::COM_END);

    MessageHandler::read_command(&conn); // ANS_CREATE_NG;

    size_t ans2 = MessageHandler::read_command(&conn); // ANS_ACK or NAK.
    if(ans2 == Protocol::ANS_ACK) {
        cout << "Newsgroup " << newsgroup_name << " created." << endl;
        MessageHandler::read_command(&conn); // ANS_END
    } else if ( ans2 == Protocol::ANS_NAK ){
        cout << "Newsgroup already exists: " ;
        cout << MessageHandler::read_command(&conn); // ERR_NG_EXISTS..
        MessageHandler::read_command(&conn); // ANS_END
    }
}
void create_article(Connection& conn,size_t& newsgroup_id, string& title, string& author, string& text){

    MessageHandler::write_command(&conn, Protocol::COM_CREATE_ART);
    MessageHandler::write_command(&conn, Protocol::PAR_NUM);
    MessageHandler::write_number(&conn, newsgroup_id);

    MessageHandler::write_command(&conn, Protocol::PAR_STRING);
    MessageHandler::write_string(&conn, title);

    MessageHandler::write_command(&conn, Protocol::PAR_STRING);
    MessageHandler::write_string(&conn, author);

    MessageHandler::write_command(&conn, Protocol::PAR_STRING);
    MessageHandler::write_string(&conn, text);
    MessageHandler::write_command(&conn, Protocol::COM_END);


    // Expect answer:
    size_t create_art = MessageHandler::read_command(&conn); // ANS_CREATE_ART
    if ( create_art == Protocol::ANS_CREATE_ART ) {
        size_t incoming = MessageHandler::read_command(&conn); // ACK or NAK
            if( incoming == Protocol::ANS_NAK) { // Could not create article
                cout << "Could not create article. Does newsgroup exist?" << endl;
             size_t error = MessageHandler::read_command(&conn); // ERR_NG_DOES_NOT_EXIST
             cout << "Error code: " << error << endl;
        } else if (incoming == Protocol::ANS_ACK ) {
            cout << "Created article " <<"'"<< title <<"'"<< endl;
        }
        MessageHandler::read_command(&conn);
    } else {
        cout << "Malformed response" << endl;
    }
}
void list_articles(Connection& conn,size_t& id){
    MessageHandler::write_command(&conn, Protocol::COM_LIST_ART);
    MessageHandler::write_command(&conn, Protocol::PAR_NUM);
    MessageHandler::write_number(&conn, id);
    MessageHandler::write_command(&conn, Protocol::COM_END);
    MessageHandler::read_command(&conn); //Always ANS_LIST_ART

    int ans = MessageHandler::read_command(&conn);
    if (ans == Protocol::ANS_ACK) {
        MessageHandler::read_command(&conn);
        size_t size = MessageHandler::read_number(&conn);
        vector<Article> articles;
        for (size_t i = 0; i < size; ++i) {
            Article a;
            MessageHandler::read_command(&conn); //par_num
            a.id = MessageHandler::read_number(&conn);
            MessageHandler::read_command(&conn); //par_string
            a.title = MessageHandler::read_string(&conn);
            articles.push_back(a);
        }
        MessageHandler::read_command(&conn); // ANS_END
        for( Article art : articles ) {
            cout << art.id << ". " << art.title << endl;
        }
    } else { 
        int error = MessageHandler::read_command(&conn);
        cout << "Article or newsgroup doesn't exist. "; 
        cout << error << endl;
        MessageHandler::read_command(&conn); // ANS_END
    }
}

void list_newsgroups(Connection& conn){

    MessageHandler::write_command(&conn, Protocol::COM_LIST_NG);
    MessageHandler::write_command(&conn, Protocol::COM_END);


    MessageHandler::read_command(&conn); // ANS_LIST_NG
    vector<Newsgroup> newsgroups;
    MessageHandler::read_command(&conn);
    size_t n = MessageHandler::read_number(&conn);
    if(n == 0) {
        cout << "No newsgroups " << endl;
        MessageHandler::read_command(&conn); // ANS_END
    } else {
        cout << "Listing all newsgroups:" << endl;
        for (size_t i = 0; i < n; ++i) {
            MessageHandler::read_command(&conn); //par_num
            size_t id = MessageHandler::read_number(&conn);
            MessageHandler::read_command(&conn); //par_string
            string name = MessageHandler::read_string(&conn);
            Newsgroup ng(name, id);
            newsgroups.push_back(ng);
        }
        MessageHandler::read_command(&conn); // ANS_END
        for(Newsgroup ng : newsgroups) {
            cout << ng.id << ". " << ng.name<< endl;
        }
    }
}
void get_article(Connection& conn,size_t& newsgroup_id, size_t& article_id){


    MessageHandler::write_command(&conn, Protocol::COM_GET_ART);
    MessageHandler::write_command(&conn, Protocol::PAR_NUM);
    MessageHandler::write_number(&conn, newsgroup_id);
    MessageHandler::write_command(&conn, Protocol::PAR_NUM);
    MessageHandler::write_number(&conn, article_id);
    MessageHandler::write_command(&conn, Protocol::COM_END);



    size_t answer = MessageHandler::read_command(&conn);
    if( answer == Protocol::ANS_GET_ART){
        size_t response = MessageHandler::read_command(&conn); // ACK or NAK.
        if(response == Protocol::ANS_ACK ) {
            string title, author, text;
            MessageHandler::read_command(&conn);
            title = MessageHandler::read_string(&conn);
            MessageHandler::read_command(&conn);
            author = MessageHandler::read_string(&conn);
            MessageHandler::read_command(&conn);
            text = MessageHandler::read_string(&conn);
            cout << "Showing article: " << title << " by " << author << ":" << endl;
            cout << text << endl;
            MessageHandler::read_command(&conn); // END
        } else if (response == Protocol::ANS_NAK) {
            size_t reason = MessageHandler::read_command(&conn);
            if(reason == Protocol::ERR_NG_DOES_NOT_EXIST) {
                cout << "Error: Newsgroup does not exist: " << reason << endl;
            } else if(reason == Protocol::ERR_ART_DOES_NOT_EXIST){
                cout << "Error: Article does not exist: " << reason << endl;
            }
            MessageHandler::read_command(&conn);
        }
    } else {
        cout << "Received malformed response : " << answer << endl; 
    }

}
void delete_newsgroup(Connection& conn,size_t& newsgroup_id) {

    MessageHandler::write_command(&conn, Protocol::COM_DELETE_NG);
    MessageHandler::write_command(&conn, Protocol::PAR_NUM);
    MessageHandler::write_number(&conn, newsgroup_id); 
    MessageHandler::write_command(&conn, Protocol::COM_END);
    size_t response = MessageHandler::read_command(&conn);
    if(response == Protocol::ANS_DELETE_NG){
        size_t ans = MessageHandler::read_command(&conn);
        if(ans == Protocol::ANS_ACK){
            cout << "Newsgroup " << newsgroup_id << " was deleted" << endl;
            MessageHandler::read_command(&conn);
        } else {
            size_t error = MessageHandler::read_command(&conn);
            cout << "Error: " << error << endl;
            MessageHandler::read_command(&conn);
        }
    } else {
        cout << "Malformed response:" << response << endl;
        cout << MessageHandler::read_command(&conn) << endl;
    }
}

void delete_article(Connection& conn, size_t& newsgroup_id, size_t& article_id) {
    MessageHandler::write_command(&conn, Protocol::COM_DELETE_ART);
    MessageHandler::write_command(&conn, Protocol::PAR_NUM);
    MessageHandler::write_number(&conn, newsgroup_id);
    MessageHandler::write_command(&conn, Protocol::PAR_NUM);
    MessageHandler::write_number(&conn, article_id);
    MessageHandler::write_command(&conn, Protocol::COM_END);
    size_t response = MessageHandler::read_command(&conn);
    if(response == Protocol::ANS_DELETE_ART){
        size_t ans = MessageHandler::read_command(&conn);
        if(ans == Protocol::ANS_ACK){
            cout << "Article " << article_id<< " in newsgroup " << newsgroup_id << " was deleted" << endl;
            MessageHandler::read_command(&conn);
        } else { 
            size_t error = MessageHandler::read_command(&conn);
            if(error == Protocol::ERR_NG_DOES_NOT_EXIST) {
                cout << "Newsgroup does not exist: " << error << endl;
                MessageHandler::read_command(&conn); // end
            } else {
                cout << "Article does not exist: " << error << endl;
                MessageHandler::read_command(&conn);
            }
        }
        cout << "Malformed response:" << response << endl;
    }
}


void help() {
    cout << " List of available commands: " << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    cout << "    create newsgroup" << endl;
    cout << "           creates a newsgroup to which you can post articles" << endl;
    cout << "           ex: create newsgroup [ENTER]" << endl;
    cout << "           newsgroupname [ENTER]" << endl;
    cout << endl;

    cout << "    create article" << endl;
    cout << "           Articles are created by supplying newsgroup ID for article " << endl;
    cout << "           as well as title, author and text. Fields are separated by carriage return " << endl;
    cout << "           ex: create article [ENTER]" << endl;
    cout << "               10 [ENTER] " << endl;
    cout << "               The Importance of Eating Breakfast [ENTER] " << endl;
    cout << "               Lord Byron [ENTER] " << endl;
    cout << "               The importance of breakfast is huge.  [ENTER] " << endl;
    cout << endl;

    cout << "    list newsgroups" << endl;
    cout << "           lists all newsgroups " << endl;
    cout << endl;

    cout << "    list articles " << endl;
    cout << "           lists all articles for a given newsgroup ID" << endl;
    cout << "           ex: list articles [ENTER]" << endl;
    cout << "           2 [ENTER]" << endl;
    cout << endl;

    cout << "    delete article " << endl;
    cout << "           deletes a given article for a given newsgroup" << endl;
    cout << "           ex: delete [ENTER] " << endl;
    cout << "           1 [ENTER] " << endl;
    cout << "           2 [ENTER] " << endl;
    cout << endl;
    cout << "    delete newsgroup " << endl;
    cout << "           deletes a given newsgroup" << endl;
    cout << "           ex: delete [ENTER] 10 [ENTER] would delete the entire newsgroup" << endl;
    cout << endl;
    cout << "    get article " << endl;
    cout << "           retrieves an article from a given newsgroup" << endl;
    cout << "           ex: get article [ENTER] 5 [ENTER] 9 [ENTER]" << endl;
    cout << endl;

    cout << "    quit" << endl;
    cout << "           Exits the usenet terminal. Also 'exit' " << endl;
    cout << endl;


}
int main(int argc, char* argv[]) {

    if (argc != 3) {
        cerr << "Usage: myclient host-name port-number" << endl;
        exit(1);
    }

    int port = -1;
    try {
        port = stoi(argv[2]);
    } catch (exception& e) {
        cerr << "Wrong port number. " << e.what() << endl;
        exit(1);
    }

    Connection conn(argv[1], port);
    if (!conn.isConnected()) {
        cerr << "Connection attempt failed" << endl;
        exit(1);
    }

    cout << "Welcome to this Usenet Terminal. Write -h or help to find out about available commands" << endl;
    string input;
    while (true) {
        try {
            getline(cin, input);
            transform(input.begin(),input.end(), input.begin(), ::tolower);

            if(input == "help" || input == "-h"){
                help();
            }
            else if (input == "quit" || input == "exit"){ //quit
                cout << "Exiting .. " << endl;
                exit(1);
            }
            else if (input == "list articles") { //list articles
                string id;
                cout << " Specify newsgroup ID : " << endl;
                getline(cin,id);
                size_t parsed_id = 1;
                try {
                    parsed_id = stoi(id);
                    list_articles(conn, parsed_id);
                } catch ( invalid_argument ia) {
                    cout << " Wrong argument: " <<ia.what() << endl;
                }
            }
            else if(input == "list newsgroups") {//list newsgroups
                list_newsgroups(conn);
            }
            else if(input == "get article") { //get_article
                string article_id, newsgroup_id;
                getline(cin, newsgroup_id); 
                cout << "Specify newsgroup ID: " << endl;
                getline(cin, article_id);
                cout << "Specify article ID: " << endl;
                getline(cin, newsgroup_id);
                size_t parsed_id = 1;
                size_t parsed_id2 = 1;
                try { 
                    parsed_id = stoi(newsgroup_id);
                    parsed_id2 = stoi(article_id);
                    get_article(conn, parsed_id,parsed_id2);
                } catch (invalid_argument ia) {
                    cout << "Wrong argument: " << ia.what() << endl;
                }
            }
            else if(input == "create newsgroup"){ //create newsgroup

                cout << "Specify name of newsgroup: " << endl;
                string name;
                getline(cin,name);
                create_newsgroup(conn, name);
            }
            else if (input == "create article") {//create article
                cout << "Specify newsgroup ID : " << endl;
                string title, author, text;
                string id;
                getline(cin, id);
                cout << "Specify title : " << endl;
                getline(cin, title);
                cout << "Specify author : " << endl;
                getline(cin, author);
                cout << "Specify text: " << endl;
                getline(cin, text);
                size_t parsed_id = 1;
                try { 
                    parsed_id = stoi(id);
                    create_article(conn, parsed_id,title,author,text);
                } catch(invalid_argument ia){
                    cout << " Wrong argument: " << ia.what() << endl;
                    cout << "Could not create article: Failed to parse newsgroup ID" << endl;
                } 
            }

            else if (input == "delete newsgroup") { //delete newsgroup
                string id;
                cout << "Specify newsgroup ID: " << endl;
                getline(cin,id);
                size_t parsed_id = 1;
                try {
                    parsed_id = stoi(id);
                    delete_newsgroup(conn, parsed_id);
                } catch (invalid_argument ia) {
                    cout << "Wrong argument: " << ia.what() << endl;
                }
            }
            else if (input == "delete article") { //delete article
                string id,id2;
                cout << "Specify newsgroup ID: " << endl;
                getline(cin,id);
                cout << "Specify article ID: " << endl;
                getline(cin,id2);
                size_t parsed_id1 = 1;
                size_t parsed_id2 = 1;
                try {
                    parsed_id1 = stoi(id);
                    parsed_id2 = stoi(id2);
                    delete_article(conn, parsed_id1,parsed_id2);
                } catch (invalid_argument ia) {
                    cout << "Wrong argument: " << ia.what() << endl;
                }

            }
            else { 
                cout << "Unknown command. Write -h or help to view available commands" << endl;
            } 

        }
        catch (ConnectionClosedException&) {
            cout << "Server unreachable. Quitting." << endl;
            exit(1);
        }
    }
}
