/* myserver.cc: sample server program */
#include "../server.h"
#include "../connection.h"
#include "../connectionclosedexception.h"
#include "../protocol.h"
#include "messagehandler.h"
#include <memory>
#include <utility>
#include <iostream>
#include <string>
#include <stdexcept>
#include <cstdlib>
#include <algorithm>
#include "newsgroup.h"
#include "database_interface.h"
#include "database_memory.h"
#include "database_file.h"
using namespace std;


int main(int argc, char* argv[]){
    if (argc != 3) {
        cerr << "Usage: myserver port-number [-f(ile) | -m(emory)]" << endl;
        exit(1);
    }

    int port = -1;
    try {
        port = stoi(argv[1]);
    } catch (exception& e) {
        cerr << "Wrong port number. " << e.what() << endl;
        exit(1);
    }
    string file_memory;
    unique_ptr<DatabaseInterface> db;
    try {
        file_memory = argv[2];
        if(file_memory.compare("-file") == 0 || file_memory.compare("-f") == 0 ) {
            db.reset(new DatabaseFile);
            cout << "Initializing file system storage.." << endl;
        } else if (file_memory.compare("-memory") == 0 || file_memory.compare("-m") == 0 ) {
            db.reset(new DatabaseMemory);
            cout << "Initializing memory storage.." << endl;
        } else {
            cerr << "Specify saving on disk with '-f' or in memory with '-m' " << endl;
            exit(1);
        }
    } catch ( exception& e ) {
        cerr << "Specify saving on disk or in memory" << endl;
        exit(1);
    }

    Server server(port);
    if (!server.isReady()) {
        cerr << "Server initialization error." << endl;
        exit(1);
    }

    while (true) {
        auto conn = server.waitForActivity();
        if (conn != nullptr) {
            try {
                MessageHandler mh; 
                string result;
                size_t nbr = mh.read_command(conn.get());
                switch(nbr) {

                    case(Protocol::COM_LIST_NG): {
                                                     size_t end = mh.read_command(conn.get());
                                                     cout << "LISTNG: server received COM_LIST_NG" << endl;
                                                     if( end == Protocol::COM_END) {
                                                         cout << "LISTNG: server received COM_END" << endl;
                                                         mh.write_command(conn.get(), Protocol::ANS_LIST_NG);
                                                         mh.write_command(conn.get(), Protocol::PAR_NUM);
                                                         cout << "LISTNG: server wrote ANS_LIST_NG" << endl;
                                                         vector<Newsgroup> results = db->list_newsgroups();
                                                         mh.write_number(conn.get(), results.size());
                                                         cout << "LISTNG: Server sent list of " << results.size() << " newsgroups! " << endl;
                                                         for( Newsgroup& n : results ) {
                                                             mh.write_command(conn.get(), Protocol::PAR_NUM);
                                                             mh.write_number(conn.get(), n.id);
                                                             mh.write_command(conn.get(), Protocol::PAR_STRING);
                                                             mh.write_string(conn.get(), n.name);
                                                         }
                                                     } 
                                                     mh.write_command(conn.get(), Protocol::ANS_END);
                                                     break;
                                                 }
                    case(Protocol::COM_CREATE_NG): { 
                                                       size_t par_str = mh.read_command(conn.get());
                                                       string name = mh.read_string(conn.get());   
                                                       size_t end2 = mh.read_command(conn.get()); // does nothing..
                                                       if( end2 == Protocol::COM_END ) {
                                                           cout << "CREATENG: Server received COM_END" << endl;
                                                           mh.write_command(conn.get(), Protocol::ANS_CREATE_NG);
                                                           if (db->create_newsgroup(name)) { 
                                                               mh.write_command(conn.get(), Protocol::ANS_ACK);
                                                               cout << "CREATENG: newsgroup was successfully created " << endl;
                                                           } else {
                                                               mh.write_command(conn.get(), Protocol::ANS_NAK);
                                                               cout << "CREATENG: newsgroup could not be created. Already exists?" << endl;
                                                               mh.write_command(conn.get(), Protocol::ERR_NG_ALREADY_EXISTS);
                                                           }
                                                           mh.write_command(conn.get(), Protocol::ANS_END);
                                                       }
                                                       break;
                                                   }
                    case(Protocol::COM_DELETE_NG): 
                                                   {  
                                                       size_t par_num = mh.read_command(conn.get());
                                                       size_t num_p = mh.read_number(conn.get());
                                                       cout <<"DELETENG: Server read " << num_p << endl;
                                                       size_t end = mh.read_command(conn.get()); // does nothing atm.

                                                       bool result = db->delete_newsgroup(num_p);
                                                       mh.write_command(conn.get(), Protocol::ANS_DELETE_NG);
                                                       if ( !result ) {
                                                           cout << "DELETENG: No such newsgroup (" << num_p << ")" << endl;
                                                           mh.write_command(conn.get(), Protocol::ANS_NAK);
                                                           mh.write_command(conn.get(), Protocol::ERR_NG_DOES_NOT_EXIST);
                                                       } else {
                                                           mh.write_command(conn.get(), Protocol::ANS_ACK);
                                                       }
                                                       mh.write_command(conn.get(), Protocol::ANS_END); 
                                                       break;
                                                   }
                    case(Protocol::COM_LIST_ART): {
                                                      cout << "LISTART: Server received COM_LIST_ART " << endl;
                                                      size_t par_num = mh.read_command(conn.get());
                                                      size_t num_p = mh.read_number(conn.get());
                                                      cout << "LISTART: Server read : " << num_p << endl;
                                                      mh.read_command(conn.get()); // does nothing atm.

                                                      mh.write_command(conn.get(), Protocol::ANS_LIST_ART);
                                                      vector<Newsgroup> newsgroups = db->list_newsgroups();
                                                      if(find_if(newsgroups.begin(), newsgroups.end(), [&] (Newsgroup ng) {return ng.id == num_p;}) != newsgroups.end()) { 
                                                          vector<Article> articles = db->list_articles(num_p);
                                                          cout << "LISTART: Server sent articles" << endl;
                                                          mh.write_command(conn.get(), Protocol::ANS_ACK);
                                                          mh.write_command(conn.get(), Protocol::PAR_NUM);
                                                          mh.write_number(conn.get(), articles.size());
                                                          for ( Article& a : articles ) {
                                                              mh.write_command(conn.get(), Protocol::PAR_NUM);
                                                              mh.write_number(conn.get(), a.id);
                                                              mh.write_command(conn.get(), Protocol::PAR_STRING);
                                                              mh.write_string(conn.get(), a.title);
                                                          }
                                                          mh.write_command(conn.get(), Protocol::ANS_END);
                                                          break;
                                                      } else {
                                                          cout << "LISTART: Server reports no such newsgroup (" <<num_p<< ")" << endl;
                                                          mh.write_command(conn.get(), Protocol::ANS_NAK);
                                                          mh.write_command(conn.get(), Protocol::ERR_NG_DOES_NOT_EXIST);
                                                          mh.write_command(conn.get(), Protocol::ANS_END);
                                                          break;
                                                      }
                                                  }

                    case(Protocol::COM_CREATE_ART):{
                                                       cout << "CREATEART: Server received COM_CREATE_ART " << endl;
                                                       size_t par_num = mh.read_command(conn.get());
                                                       size_t newsgroup_id = mh.read_number(conn.get());
                                                       cout << "CREATEART: Server received newsgroup id : " << newsgroup_id << endl;

                                                       size_t par_str = mh.read_command(conn.get());
                                                       string title = mh.read_string(conn.get());
                                                       cout << "CREATEART: Server received title : " << title << endl;
                                                       par_str = mh.read_command(conn.get());
                                                       string author = mh.read_string(conn.get());
                                                       cout << "CREATEART: Server received author : " << author << endl;
                                                       par_str = mh.read_command(conn.get());
                                                       string text = mh.read_string(conn.get());
                                                       cout << "CREATEART: Server received  text : " << text << endl;

                                                       size_t end = mh.read_command(conn.get());
                                                       cout << "CREATEART: Server received end  : " << end << endl;
                                                       mh.write_command(conn.get(), Protocol::ANS_CREATE_ART);
                                                       bool result = db->create_article(newsgroup_id, title, author, text);
                                                       if ( !result ) {
                                                           mh.write_command(conn.get(), Protocol::ANS_NAK);
                                                           mh.write_command(conn.get(), Protocol::ERR_NG_DOES_NOT_EXIST);
                                                           mh.write_command(conn.get(), Protocol::ANS_END);
                                                           cout << "CREATEART: Server could not create article. NG doesnt exist? " << endl;
                                                           break;
                                                       } else { 
                                                           mh.write_command(conn.get(), Protocol::ANS_ACK);
                                                           mh.write_command(conn.get(), Protocol::ANS_END);
                                                           cout << "CREATEART: Successfully created article, sent ACK, END" << endl;
                                                           break;
                                                       }
                                                   }

                    case(Protocol::COM_DELETE_ART): {
                                                        size_t num_p = mh.read_command(conn.get()); // PAR_NUM
                                                        size_t newsgroup_id = mh.read_number(conn.get()); // N
                                                        num_p = mh.read_command(conn.get()); // PAR_NUM
                                                        size_t article_id = mh.read_number(conn.get());
                                                        size_t end = mh.read_command(conn.get());
                                                        mh.write_command(conn.get(), Protocol::ANS_DELETE_ART);
                                                        int result = db->delete_article(newsgroup_id, article_id);

                                                        if(result == 0 ) {// Both exist
                                                            cout << "DELART: Server reports successful delete" << endl;
                                                            mh.write_command(conn.get(), Protocol::ANS_ACK);
                                                            mh.write_command(conn.get(), Protocol::ANS_END);
                                                            break;
                                                        } else if (result == -1 ) { // ng missing
                                                            cout << "DELART: Server reports no such newsgroup (" <<newsgroup_id << ")" << endl;
                                                            mh.write_command(conn.get(),Protocol::ANS_NAK);
                                                            mh.write_command(conn.get(), Protocol::ERR_NG_DOES_NOT_EXIST);
                                                            mh.write_command(conn.get(), Protocol::ANS_END);
                                                            break;

                                                        } else if (result == -2 ) { // Article doesn't exist
                                                            cout << "DELART: Server reports no such article (" <<article_id<< ")" << endl;
                                                            mh.write_command(conn.get(),Protocol::ANS_NAK);
                                                            mh.write_command(conn.get(),Protocol::ERR_ART_DOES_NOT_EXIST);
                                                            mh.write_command(conn.get(),Protocol::ANS_END);
                                                            break;
                                                        }
                                                        // Not gonna happen.
                                                        break;
                                                    }
                    case(Protocol::COM_GET_ART): { 
                                                     size_t num_p = mh.read_command(conn.get()); // PAR_NUM
                                                     size_t newsgroup_id = mh.read_number(conn.get()); // N
                                                     cout << "GETART: Server read " << newsgroup_id << " as newsgr id " << endl;
                                                     num_p = mh.read_command(conn.get()); // PAR_NUM
                                                     size_t article_id = mh.read_number(conn.get());
                                                     cout << "GETART: Server read " << article_id << " as arti id " << endl;
                                                     size_t end = mh.read_command(conn.get());
                                                     pair<int, Article> the_pair = db->get_article(newsgroup_id, article_id);
                                                     Article the_article = the_pair.second;
                                                     mh.write_command(conn.get(), Protocol::ANS_GET_ART);
                                                     if ( the_pair.first == 1 ) {
                                                         cout << "GETART: Server reports article was successfully delivered" << endl;
                                                         mh.write_command(conn.get(), Protocol::ANS_ACK);

                                                         mh.write_command(conn.get(), Protocol::PAR_STRING);
                                                         mh.write_string(conn.get(), the_article.title);

                                                         mh.write_command(conn.get(), Protocol::PAR_STRING);
                                                         mh.write_string(conn.get(), the_article.author);

                                                         mh.write_command(conn.get(), Protocol::PAR_STRING);
                                                         mh.write_string(conn.get(), the_article.text);

                                                         mh.write_command(conn.get(), Protocol::ANS_END);
                                                         break;
                                                     } else if ( the_pair.first == 0 ) { 
                                                         cout << "GETART: Server reports no such newsgroup (" <<newsgroup_id<<")" << endl;
                                                         mh.write_command(conn.get(), Protocol::ANS_NAK);
                                                         mh.write_command(conn.get(), Protocol::ERR_NG_DOES_NOT_EXIST);
                                                         mh.write_command(conn.get(), Protocol::ANS_END);
                                                         break;
                                                     } else {
                                                         cout << "GET: Server reports no such article(" <<article_id<<")" << endl;
                                                         mh.write_command(conn.get(), Protocol::ANS_NAK);
                                                         mh.write_command(conn.get(), Protocol::ERR_ART_DOES_NOT_EXIST);
                                                         mh.write_command(conn.get(), Protocol::ANS_END);
                                                         break; 
                                                     }

                                                 } 
                    default: {
                                 cerr << "Received malformed request" << endl;
                                 break;
                             }
                }
            } catch (ConnectionClosedException&) {
                server.deregisterConnection(conn);
                cout << "Client closed connection" << endl;
            }
        } else {
            conn = make_shared<Connection>();
            server.registerConnection(conn);
            cout << "New client connects" << endl;
        }
    }
}

