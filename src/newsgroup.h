
#ifndef NEWSGROUP_H
#define NEWSGROUP_H

#include <string>


struct Newsgroup {
    
    Newsgroup(std::string _name, size_t _id) : name(_name), id(_id) {};
    std::string name;
    size_t id;

};
#endif
